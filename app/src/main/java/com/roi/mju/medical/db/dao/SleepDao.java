package com.roi.mju.medical.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.*;
import io.reactivex.Flowable;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface SleepDao {
    @Query("SELECT * FROM SleepData")
//    LiveData<ArrayList<SleepData>> selectAll();
    SleepData selectAll();

    @Query("SELECT * FROM SleepData")
    List<SleepData> getAllSleepData();

    @Query("SELECT * FROM SleepData WHERE currentDate=:date")
    SleepData getSleepData(String date);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSleepData(SleepData... data);

    @Query(value = "DELETE FROM SleepData WHERE  currentDate=:date")
    int deleteSleepData(String date);

    @Query("DELETE FROM SleepData")
    void clearAll();
}

//E/jun: mSleepCurrentDate 날짜 = 201997
//        2019-09-08 12:26:55.799 6000-6000/com.roi.mju.medical E/jun: mSleepReplaceDate 날짜 = 2019년 09월 07일
//        2019-09-08 12:26:55.800 6000-18529/com.roi.mju.medical E/jun: SleepDay 선택된 날짜 = 201997
//        2019-09-08 12:26:55.801 6000-18529/com.roi.mju.medical E/jun: SleepDay 잠자리에 누운시간 = 3600
//        2019-09-08 12:26:55.801 6000-18529/com.roi.mju.medical E/jun: SleepDay 언제 잠이 들었나요 = 7200
//        2019-09-08 12:26:55.801 6000-18529/com.roi.mju.medical E/jun: SleepDay 언제 잠이 깼나요 = 14400
//        2019-09-08 12:26:55.801 6000-18529/com.roi.mju.medical E/jun: SleepDay 언제 잠자리에서 나왔나요 = 18000
//        2019-09-08 12:26:55.801 6000-18529/com.roi.mju.medical E/jun: SleepDay goHour = 1 startHour = 2
//        2019-09-08 12:26:55.801 6000-18529/com.roi.mju.medical E/jun: SleepDay endHour = 4 wentHour = 5
//        2019-09-08 12:26:56.301 6000-6000/com.roi.mju.medical E/jun: ############initSettingDataView#######################
//        2019-09-08 12:26:56.301 6000-6000/com.roi.mju.medical E/jun: mStartSleepIndex = 7
//        2019-09-08 12:26:56.301 6000-6000/com.roi.mju.medical E/jun: mEndSleepIndex = 9
//        2019-09-08 12:26:56.301 6000-6000/com.roi.mju.medical E/jun: #################initSettingDataView##################
//        2019-09-08 12:26:56.301 6000-6000/com.roi.mju.medical E/jun: ##########initSleepDayView#########################
//        2019-09-08 12:26:56.301 6000-6000/com.roi.mju.medical E/jun: goTime = 1시 0분
//        2019-09-08 12:26:56.301 6000-6000/com.roi.mju.medical E/jun: startTime = 2시 0분
//        2019-09-08 12:26:56.301 6000-6000/com.roi.mju.medical E/jun: endTime = 4시 0분
//        2019-09-08 12:26:56.301 6000-6000/com.roi.mju.medical E/jun: wentTime = 5시 0분
//        2019-09-08 12:26:56.301 6000-6000/com.roi.mju.medical E/jun: ############initSleepDayView#######################
//        2019-09-08 12:26:56.302 6000-6000/com.roi.mju.medical E/jun: percent = 200
//        2019-09-08 12:26:56.302 6000-6000/com.roi.mju.medical E/jun: day percent = 200
//        2019-09-08 12:30:56.603 6000-19535/com.roi.mju.medical E/jun: ------------------------------ DB 저장 ------------------------------
//        2019-09-08 12:30:56.603 6000-19535/com.roi.mju.medical E/jun: 저장 날짜 = 201997
//        2019-09-08 12:30:56.603 6000-19535/com.roi.mju.medical E/jun: sleepGoTime = 3600 sleepWentTime = 18000
//        2019-09-08 12:30:56.603 6000-19535/com.roi.mju.medical E/jun: sleepStartTime = 7200 sleepEndTime = 14400
//        2019-09-08 12:30:56.603 6000-19535/com.roi.mju.medical E/jun: sleepWentTime = 18000
//        2019-09-08 12:30:56.603 6000-19535/com.roi.mju.medical E/jun: percent = 200
//        2019-09-08 12:30:56.603 6000-19535/com.roi.mju.medical E/jun: 수면 효율 = 100
//        2019-09-08 12:30:56.603 6000-19535/com.roi.mju.medical E/jun: ------------------------------ DB 저장 ------------------------------
//        2019-09-08 12:30:56.603 6000-19535/com.roi.mju.medical E/jun: ------------------------------ DB 확인 ------------------------------
//        2019-09-08 12:30:56.604 6000-19535/com.roi.mju.medical E/jun: db데이터있다 = com.roi.mju.medical.db.dao.SleepData@925efbf
//        2019-09-08 12:30:56.604 6000-19535/com.roi.mju.medical E/jun: ------------------------------ DB 삭제 ------------------------------
//        2019-09-08 12:30:56.646 6000-6000/com.roi.mju.medical E/jun: mSleepCurrentDate 날짜 = 201997
//        2019-09-08 12:30:56.646 6000-6000/com.roi.mju.medical E/jun: mSleepReplaceDate 날짜 = 2019년 09월 07일
//        2019-09-08 12:30:56.648 6000-19540/com.roi.mju.medical E/jun: SleepDay 선택된 날짜 = 201997
//        2019-09-08 12:30:56.648 6000-19540/com.roi.mju.medical E/jun: SleepDay 잠자리에 누운시간 = 3600
//        2019-09-08 12:30:56.648 6000-19540/com.roi.mju.medical E/jun: SleepDay 언제 잠이 들었나요 = 7200
//        2019-09-08 12:30:56.648 6000-19540/com.roi.mju.medical E/jun: SleepDay 언제 잠이 깼나요 = 14400
//        2019-09-08 12:30:56.648 6000-19540/com.roi.mju.medical E/jun: SleepDay 언제 잠자리에서 나왔나요 = 18000
//        2019-09-08 12:30:56.648 6000-19540/com.roi.mju.medical E/jun: SleepDay goHour = 1 startHour = 2
//        2019-09-08 12:30:56.648 6000-19540/com.roi.mju.medical E/jun: SleepDay endHour = 4 wentHour = 5
//        2019-09-08 12:30:57.147 6000-6000/com.roi.mju.medical E/jun: ############initSettingDataView#######################
//        2019-09-08 12:30:57.147 6000-6000/com.roi.mju.medical E/jun: mStartSleepIndex = 7
//        2019-09-08 12:30:57.147 6000-6000/com.roi.mju.medical E/jun: mEndSleepIndex = 9
//        2019-09-08 12:30:57.147 6000-6000/com.roi.mju.medical E/jun: #################initSettingDataView##################
//        2019-09-08 12:30:57.148 6000-6000/com.roi.mju.medical E/jun: ##########initSleepDayView#########################
//        2019-09-08 12:30:57.148 6000-6000/com.roi.mju.medical E/jun: goTime = 1시 0분
//        2019-09-08 12:30:57.148 6000-6000/com.roi.mju.medical E/jun: startTime = 2시 0분
//        2019-09-08 12:30:57.148 6000-6000/com.roi.mju.medical E/jun: endTime = 4시 0분
//        2019-09-08 12:30:57.148 6000-6000/com.roi.mju.medical E/jun: wentTime = 5시 0분
//        2019-09-08 12:30:57.148 6000-6000/com.roi.mju.medical E/jun: ############initSleepDayView#######################
//        2019-09-08 12:30:57.148 6000-6000/com.roi.mju.medical E/jun: percent = 200
//        2019-09-08 12:30:57.148 6000-6000/com.roi.mju.medical E/jun: day percent = 200
