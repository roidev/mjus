package com.roi.mju.medical.activity

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.roi.mju.medical.R
import com.roi.mju.medical.find.FindFirstFragments
import com.roi.mju.medical.find.FindFourthFragment
import com.roi.mju.medical.find.FindSecondFragment
import com.roi.mju.medical.find.FindThirdFragment
import com.roi.mju.medical.instructions.*
import me.relex.circleindicator.CircleIndicator

/**
 * 수면 지침
 */
class SleepInstructionsMainActivity : AppCompatActivity() {
//    private var mBinding: ActivitySleepInstructionsMainBinding? = null
    var adapterViewPager: FragmentPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sleep_instructions_main)
//        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_sleep_instructions_main)

        val vpPager = findViewById(R.id.vpPager) as ViewPager
        adapterViewPager = MyPagerAdapter(supportFragmentManager, this)
        vpPager.adapter = adapterViewPager

        val indicator = findViewById(R.id.indicator) as CircleIndicator
        indicator.setViewPager(vpPager)
    }

    class MyPagerAdapter(fragmentManager: FragmentManager, context: Context) : FragmentPagerAdapter(fragmentManager) {
        var mContext = context

        override fun getCount(): Int {
            return NUM_ITEMS
        }

        // Returns the fragment to display for that page
        override fun getItem(position: Int): Fragment {
            when (position) {
                0 -> return InstructionsFirstFragment.newInstance(0, mContext.resources.getString(R.string.instructions_sleep_page_01))
                1 -> return InstructionsSecondFragment.newInstance(1, mContext.resources.getString(R.string.instructions_sleep_page_02))
                2 -> return InstructionsThirdFragment.newInstance(2, mContext.resources.getString(R.string.instructions_sleep_page_03))
                3 -> return InstructionsFourthFragment.newInstance(3, mContext.resources.getString(R.string.instructions_sleep_page_04))
                4 -> return InstructionsFifthFragment.newInstance(4, mContext.resources.getString(R.string.instructions_sleep_page_05))
                5 -> return InstructionsSixthFragment.newInstance(5, mContext.resources.getString(R.string.instructions_sleep_page_06))
                6 -> return InstructionsSeventhFragment.newInstance(6, mContext.resources.getString(R.string.instructions_sleep_page_07))
                7 -> return InstructionsEighthFragment.newInstance(7, mContext.resources.getString(R.string.instructions_sleep_page_08))
                else -> return InstructionsFirstFragment.newInstance(0, "Page # 1")
            }
        }

        // Returns the page title for the top indicator
        override fun getPageTitle(position: Int): CharSequence? {
            return "Page $position"
        }

        companion object {
            private val NUM_ITEMS = 8
        }
    }
}
