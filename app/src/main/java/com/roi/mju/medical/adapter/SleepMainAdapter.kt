package com.roi.mju.medical.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.roi.mju.medical.R
import com.roi.mju.medical.base.navigator.Navigator

class SleepMainAdapter(private val mContext: Context, sleepMainList : MutableList<SleepMainVO>) : RecyclerView.Adapter<SleepMainAdapter.SleepMainViewHolder>() {
    private var mSleepMainList: MutableList<SleepMainVO>? = sleepMainList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SleepMainViewHolder {
        var view = LayoutInflater.from(parent!!.context).inflate(R.layout.item_main, parent, false)
        return SleepMainViewHolder(view)
    }

    override fun onBindViewHolder(holder: SleepMainViewHolder, position: Int) {
        var sleepVO = mSleepMainList!![position]

        holder!!.sleepTitle.text = sleepVO.title
        setItemBackground(holder, sleepVO.type)
    }

    override fun getItemCount(): Int {
        return if (mSleepMainList == null) 0 else mSleepMainList!!.size
    }

    fun setItemBackground(viewHolder: SleepMainViewHolder, type: Int) {
        if (type == 0) {
            viewHolder.itemView.setBackgroundResource(R.drawable.sleep2)
            viewHolder.itemView.setOnClickListener { View ->   Navigator.get().moveToSleepInstructions() }
        } else if (type == 1) {
            viewHolder.itemView.setBackgroundResource(R.drawable.sleep8)
            viewHolder.itemView.setOnClickListener { View ->   Navigator.get().moveToSleeInitDiary() }
        } else if (type == 2) {
            viewHolder.itemView.setBackgroundResource(R.drawable.sleep4)
            viewHolder.itemView.setOnClickListener { View ->   Navigator.get().moveToRelaxationTherapy() }
        } else if (type == 3) {
            viewHolder.itemView.setBackgroundResource(R.drawable.sleep)
            viewHolder.sleepTitle.setTextColor(mContext.resources.getColor(R.color.colorBlack))
            viewHolder.itemView.setOnClickListener { View ->   Navigator.get().moveToSleepMyFind() }
        } else if (type == 4) {
            viewHolder.itemView.setBackgroundResource(R.drawable.sleep9)
            viewHolder.sleepTitle.setTextColor(mContext.resources.getColor(R.color.colorBlack))
            viewHolder.itemView.setOnClickListener { View ->   Navigator.get().moveToSleepInformation() }
        } else if (type == 5) {
            viewHolder.itemView.setBackgroundResource(R.drawable.sleep1)
            viewHolder.itemView.setOnClickListener { View ->   Navigator.get().moveToSleepMyInstructions() }
        }
    }

    class SleepMainViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var sleepTitle = itemView.findViewById(R.id.main_item_title) as TextView
    }
}


