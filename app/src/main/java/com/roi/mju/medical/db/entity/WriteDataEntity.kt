package com.roi.mju.medical.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.roi.mju.medical.db.dao.SleepData

@Entity(tableName = "writeSleep")
data class WriteDataEntity(@PrimaryKey(autoGenerate = true) val id: Long, var sleepData: ArrayList<SleepData>, var date: String)
// id = pk, sleepData = index 시간 인덱스, data = 날짜