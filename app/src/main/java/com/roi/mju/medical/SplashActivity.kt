package com.roi.mju.medical

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.roi.mju.medical.base.navigator.Navigator

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        roadingTimer()
    }

    override fun onPause() {
        super.onPause()
    }

    private fun roadingTimer() {
        Handler().postDelayed(Runnable {
            // 메인 이동
            Navigator.get().moveToSleepMain()
            finish()
        }, 500)
    }
}
