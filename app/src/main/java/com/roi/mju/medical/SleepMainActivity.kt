package com.roi.mju.medical

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.roi.mju.medical.adapter.SleepMainVO
import com.roi.mju.medical.adapter.SleepMainAdapter
import com.roi.mju.medical.base.navigator.Navigator
import com.roi.mju.medical.util.CustomDialog
import com.roi.mju.medical.util.SleepUtil
import kotlinx.android.synthetic.main.activity_sleep_main.*
import kotlin.collections.arrayListOf as arrayListOf1

class SleepMainActivity : AppCompatActivity() {
    lateinit var customDialog: CustomDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sleep_main)

        var mainAdapter = SleepMainAdapter(this, initMainItemData())
        recyclerView.adapter = mainAdapter
        recyclerView.setHasFixedSize(true)
    }

    fun initMainItemData(): MutableList<SleepMainVO> {
        var mainList: MutableList<SleepMainVO> = arrayListOf1()

        var mainItem01 = SleepMainVO("수면 지침", 0)
        var mainItem02 = SleepMainVO("내 수면 일기", 1)
        var mainItem03 = SleepMainVO("이완 요법", 2)
        var mainItem04 = SleepMainVO("내 수면 찾기", 3)
        var mainItem05 = SleepMainVO("병원 정보", 4)
        var mainItem06 = SleepMainVO("내 수면 지침", 5)

        mainList.add(mainItem01)
        mainList.add(mainItem02)
        mainList.add(mainItem03)
        mainList.add(mainItem04)
        mainList.add(mainItem05)
        mainList.add(mainItem06)

        return mainList
    }

    // 수면 지침
    fun moveInstructions(view: View) {
        Navigator.get().moveToSleepInstructions()
    }

    // 이완 요법
    fun moveRelax(view: View) {
        Navigator.get().moveToRelaxationTherapy()
    }

    // 내 수면 바로 알기
    fun moveSleepMyKnow(view: View) {
        Navigator.get().moveToSleepMyKnow()
    }

    // 내 수면 일기
    fun moveSleepMyDiary(view: View) {
        Navigator.get().moveToSleeInitDiary()
//        Navigator.get().moveToSleepMyDiary()
    }

    // 내 수면 찾기
    fun moveSleepMyFind(view: View) {
        Navigator.get().moveToSleepMyFind()
    }

    // 내 수면 지침 (모아논 곳)
    fun moveSleepMyInstructions(view: View) {
        Navigator.get().moveToSleepMyInstructions()
    }

    fun showDialog(view: View) {
        Navigator.get().moveToSleepInformation()
//        customDialog = CustomDialog(this, View.OnClickListener { view -> customDialog.dismiss() })
//        customDialog.show()
    }
}
