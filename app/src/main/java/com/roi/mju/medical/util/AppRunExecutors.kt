package com.roi.mju.medical.util

import java.util.concurrent.Executors

class AppRunExecutors {
    private val IO_EXECUTOR = Executors.newSingleThreadExecutor()

    fun runOnIoThread(runnable: Runnable) {
        IO_EXECUTOR.execute(runnable)
    }
}