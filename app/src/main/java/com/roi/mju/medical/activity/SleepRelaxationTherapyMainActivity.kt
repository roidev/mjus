package com.roi.mju.medical.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.roi.mju.medical.base.navigator.Navigator
import kotlinx.android.synthetic.main.activity_relaxation_therapy_main.*
import android.media.SoundPool
import com.roi.mju.medical.R
import android.media.AudioManager
import android.media.MediaPlayer
import android.util.Log
import com.roi.mju.medical.db.dao.SleepData
import com.roi.mju.medical.util.AppRunExecutors

/**
 * 이완 요법
 */
class SleepRelaxationTherapyMainActivity : AppCompatActivity() {
    private var sound_pool: SoundPool? = null
    var mediaPlayer: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_relaxation_therapy_main)

//        sound_pool = SoundPool(5, AudioManager.STREAM_MUSIC, 0)
//        val soundBreathing = sound_pool!!.load(this, R.raw.breathing, 1)

//        - soundID : 재생시킬 파일의 resID
//        - leftVolume : 왼쪽 볼륨 크기 (range : 0.0 ~ 1.0)
//        - rightVolume : 오른쪽 볼륨 크리 (range : 0.0 ~ 1.0)
//        - priority : 우선순위 ( 0이 가장 낮음을 나타냅니다)
//        - loop : 재생횟수입니다. (0일경우 1번만 재생 -1일 경우에는 무한반복)
//        - rate : 재생속도입니다.0.5로 할 경우 2배 느리게 재생되고 2.0으로 할 경우 2배 빠르게 재생됩니다. (range : 0.5 ~ 2.0)
//        sound_pool!!.setOnLoadCompleteListener(SoundPool.OnLoadCompleteListener { soundPool, soundId, status ->
//            sound_pool!!.play(
//                soundBreathing,
//                1f,
//                1f,
//                0,
//                0,
//                1f
//            )
//        })

//        mediaPlayer = MediaPlayer.create(this, R.raw.breathing)
//        AppRunExecutors().runOnIoThread(Runnable {
//            Log.e("jun", "mediaPlayer start")
//                mediaPlayer!!.start()
//        })


        // 복식 호흡
        relax_sum.setOnClickListener { Navigator.get().moveToRelaxationSum() }
        music_sum.setOnClickListener { playMusic(R.raw.breathing)}

        // 명상
        relax_think.setOnClickListener { Navigator.get().moveToMungsang() }
        music_think.setOnClickListener { playMusic(R.raw.mungsang)}

        // 점진적 근육 이완
        relax_muscle.setOnClickListener { Navigator.get().moveToMuscle() }
        music_relax.setOnClickListener { playMusic(R.raw.muscle)}

        // 심상법
        relax_sim.setOnClickListener { Navigator.get().moveToSimsang() }
        music_sim.setOnClickListener { playMusic(R.raw.simsang)}
    }

    fun playMusic(musicId : Int) {
        if(mediaPlayer != null) {
            if(mediaPlayer?.isPlaying()!!) {
                mediaPlayer?.stop()
            }
        }

        mediaPlayer = MediaPlayer.create(this, musicId)

        AppRunExecutors().runOnIoThread(Runnable {
            Log.e("jun", "mediaPlayer start")
            mediaPlayer?.start()
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer?.release()
        mediaPlayer = null
    }
}
