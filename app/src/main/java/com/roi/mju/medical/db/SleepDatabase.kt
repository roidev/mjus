package com.roi.mju.medical.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.roi.mju.medical.db.dao.SleepDao
import com.roi.mju.medical.db.dao.SleepData
import com.roi.mju.medical.db.entity.WriteDataEntity

@Database(entities = arrayOf(SleepData::class), version = 1, exportSchema = false)
abstract class SleepDatabase : RoomDatabase() {

    companion object {
        private var INSTANCE: SleepDatabase? = null
        val DATABASE_NAME = "HappySleep"

        fun getInstance(context: Context): SleepDatabase? {
            if(INSTANCE == null) {
                synchronized(SleepDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext, SleepDatabase::class.java, DATABASE_NAME).allowMainThreadQueries().build()
                }
            }
            return INSTANCE
        }
    }

    abstract fun sleepDao(): SleepDao
}


//@Database(entities = {SleepData.class}, version = 1)
//public abstract class MjuSleepDatabase extends RoomDatabase {
//    public static final String TAG = "DATABASE";
//
//    public static final String DATABASE_NAME = "Sleep";
//
//    public static MjuSleepDatabase instatnce;
//
//    public static synchronized MjuSleepDatabase getInstance(Context context) {
//        if(instatnce == null) {
//            synchronized (MjuSleepDatabase.class) {
//                instatnce = Room.databaseBuilder(context,
//                MjuSleepDatabase.class, DATABASE_NAME)
//                .allowMainThreadQueries()
//                .build();
//            }
//        }
//        return instatnce;
//    }