package com.roi.mju.medical.activity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import com.roi.mju.medical.R;
import com.roi.mju.medical.adapter.SleepDiaryAdapter;
import com.roi.mju.medical.base.navigator.Navigator;
import com.roi.mju.medical.db.SleepDatabase;
import com.roi.mju.medical.db.dao.SleepData;
import com.roi.mju.medical.util.AppRunExecutors;
import com.roi.mju.medical.util.SleepTimePickerDialog;
import com.roi.mju.medical.util.SleepUtil;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 내 수면 일기 첫 화면
 */
public class SleepInitDiaryMainActivity extends AppCompatActivity {
    private final String TAG = "jun";
    private Context mContext;

    private SleepDatabase mSleepDatabase;

    private long sleepGoTime = -1L;   // 언제 잠자리 누웠나요?
    private long sleepStartTime = -1L;   // 언제 잠이 들었나요?
    private long sleepEndTime = -1L;   // 언제 잠이 깼나요?
    private long sleepWentTime = -1L;   // 언제 잠자리에서 나왔나요?
    private long sleepPercent = -1L;     // 수면 효율

    private long mYear, mMonth, mDay;
    private int mGoHour, mStartHour, mEndHour, mWentHour; // hour 저장

    private String sleepDateData;

    TextView sleepDate;
    Button sleep01, sleep02, sleep03, sleep04;
    Button sleepTodayButton, sleepWeekButton;

    private SleepDiaryAdapter mSleepDiaryAdapter;
    private RecyclerView mSleepDiaryListView;
    private View.OnClickListener mOnItemClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_sleep_my_diary_main);

        new AppRunExecutors().runOnIoThread(new Runnable() {
            @Override
            public void run() {
                mSleepDatabase = SleepDatabase.Companion.getInstance(mContext);
            }
        });

        initSleepDiray();
//        initSleepDayView();
    }

    private void initSleepDiray() {
        sleepDate = findViewById(R.id.sleep_date);
        sleep01 = findViewById(R.id.sleep01);
        sleep01.setTag("01");
        sleep02 = findViewById(R.id.sleep02);
        sleep02.setTag("02");
        sleep03 = findViewById(R.id.sleep03);
        sleep03.setTag("03");
        sleep04 = findViewById(R.id.sleep04);
        sleep04.setTag("04");

        sleepTodayButton = findViewById(R.id.sleep_today);
        sleepWeekButton = findViewById(R.id.sleep_week);

        // 날짜 선택
        sleepDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDatePickerDialog();
            }
        });

        sleep01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimePickerDialog(sleep01);
            }
        });

        sleep02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimePickerDialog(sleep02);
            }
        });

        sleep03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimePickerDialog(sleep03);
            }
        });

        sleep04.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimePickerDialog(sleep04);
            }
        });

        // 이날의 내 수면 그래프
        sleepTodayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sleepDateData == null || sleepGoTime == -1L || sleepStartTime == -1L || sleepEndTime == -1L || sleepWentTime == -1L) {
                    Toast.makeText(mContext, "모두 입력해주세요", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    Navigator.get().moveToSleepDay(mYear, mMonth, mDay);
                }

                new AppRunExecutors().runOnIoThread(new Runnable() {
                    @Override
                    public void run() {
//                        Log.e("jun", "------------------------------ DB 저장 ------------------------------");
//                        Log.e("jun", "저장 날짜 = " + sleepDateData);
//                        Log.e("jun", "sleepGoTime = " + sleepGoTime + " sleepWentTime = " + sleepWentTime);
//                        Log.e("jun", "sleepStartTime = " + sleepStartTime + " sleepEndTime = " + sleepEndTime);
//                        Log.e("jun", "sleepWentTime = " + sleepWentTime);

                        // 데이터베이스에 저장 (날짜 변환한걸 저장중)
                        // 수면 효율
                        // 야간 수면 효율 = 잠든 시간부터 잠깬 시간 / 누운 시간부터 일어난 시간
                        if(sleepStartTime > -1 && sleepEndTime > -1 && sleepGoTime > -1 && sleepWentTime > -1) {
                            int goHours = getTimeGap(sleepGoTime, 0, Calendar.HOUR);
                            int startHours = getTimeGap(sleepStartTime, 0, Calendar.HOUR);
                            int endHours = getTimeGap(sleepEndTime, 0, Calendar.HOUR);
                            int wentHours = getTimeGap(sleepWentTime, 0, Calendar.HOUR);

                            sleepPercent = SleepUtil.caculateSleepPercent(startHours, endHours, goHours, wentHours);
                            if(sleepPercent > 100) sleepPercent = 100;
//                            Log.e("jun", "수면 효율 = " + sleepPercent);
//                            Log.e("jun", "------------------------------ DB 저장 ------------------------------");
                        }
//                        Log.e("jun", "------------------------------ DB 확인 ------------------------------");
                        if(mSleepDatabase.sleepDao().getSleepData(sleepDateData) != null) {
//                            Log.e("jun", "db데이터있다 = " + mSleepDatabase.sleepDao().getSleepData(sleepDateData));
                            int a = mSleepDatabase.sleepDao().deleteSleepData(sleepDateData);
//                            Log.e("jun", "db데이터 삭제 = " + a);
//                            Log.e("jun", "------------------------------ DB 삭제 ------------------------------");
                        }

//                        Log.e("jun", "------------------------------ DB 삽입 ------------------------------11 = " + sleepPercent);
                        SleepData data = new SleepData(mYear, mMonth, mDay, sleepDateData, sleepGoTime, sleepStartTime, sleepEndTime, sleepWentTime, sleepPercent,
                                mGoHour, mStartHour, mEndHour, mWentHour);
                        mSleepDatabase.sleepDao().insertSleepData(data);
//                        Log.e("jun", "------------------------------ DB 삽입 ------------------------------22");
                    }
                });

//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        if(sleepDateData == null || sleepGoTime == 0L || sleepStartTime == 0L || sleepEndTime == 0L || sleepWentTime == 0L) {
//                            Toast.makeText(mContext, "모두 입력해주세요", Toast.LENGTH_SHORT).show();
//                        } else {
//                            Navigator.get().moveToSleepDay(mYear, mMonth, mDay);
//                        }
//                    }
//                }, 500);
            }
        });

        // 일주일의 내 수면 그래프
        sleepWeekButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigator.get().moveToSleepMyDiary();

            }
        });
    }

    private void showDatePickerDialog() {
        final Calendar cal = Calendar.getInstance();

//        Log.e(TAG, cal.get(Calendar.YEAR)+"");
//        Log.e(TAG, cal.get(Calendar.MONTH)+1+"");
//        Log.e(TAG, cal.get(Calendar.DATE)+"");
//        Log.e(TAG, cal.get(Calendar.HOUR_OF_DAY)+"");
//        Log.e(TAG, cal.get(Calendar.MINUTE)+"");

        DatePickerDialog dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int date) {
//                String msg = String.format("%d 년 %d 월 %d 일", year, month+1, date);
                String msg = String.format("%d년 %d월 %d일", year, month+1, date);
                sleepDate.setText(msg); // 날짜 표시

                mYear = year;
                mMonth = month+1;
                mDay = date;

                sleepDateData = String.format("%d%d%d", year, month+1, date); // 날짜 변환
                Log.e("jun", "변환한 날짜 = " + sleepDateData);

                Toast.makeText(SleepInitDiaryMainActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        },cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
        dialog.getDatePicker().setMaxDate(new Date().getTime());
        dialog.show();
    }

    private void showTimePickerDialog(final TextView textView) {
        final Calendar cal = Calendar.getInstance();
        SleepTimePickerDialog dialog = new SleepTimePickerDialog(SleepInitDiaryMainActivity.this, android.R.style.Theme_Holo_Light_Dialog, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int min) {

                String msg = String.format("%d 시 %d 분", hour, min);
                long revertSecond = (hour * 60 * 60) + (min * 60);

                int hours = getTimeGap(revertSecond, 0, Calendar.HOUR);
                int minutes = getTimeGap(revertSecond, 0, Calendar.MINUTE);
//                Log.e("jun", "revertSecond = " + revertSecond);
//                Log.e("jun", "선택 hour = " + hours + " min = " + minutes);
                if(hour == 0) {
                    hour = 24;
                }

                // 오전 12:00 - 0으로 나옴
//                if(revertSecond == 0) {
//                    revertSecond = 43200;
//                } else if(revertSecond > 0 && 43200 > revertSecond){
//                    revertSecond = revertSecond + 43200;
//                }

                if(textView.getTag().equals("01")) {
                    sleepGoTime = revertSecond;
                    mGoHour = hour;
                } else if(textView.getTag().equals("02")) {
                    sleepStartTime = revertSecond;
                    mStartHour = hour;
                } else if(textView.getTag().equals("03")) {
                    sleepEndTime = revertSecond;
                    mEndHour = hour;
                } else if(textView.getTag().equals("04")) {
                    sleepWentTime = revertSecond;
                    mWentHour = hour;
                }

                textView.setText(msg);
                Toast.makeText(SleepInitDiaryMainActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
        }, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), false);  //마지막 boolean 값은 시간을 24시간으로 보일지 아닐지


        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.show();

//        SleepTimePickerDialog dialog = new SleepTimePickerDialog(mContext,  android.R.style.Theme_Holo_Light_Dialog, new TimePickerDialog.OnTimeSetListener() {
//            @Override
//            public void onTimeSet(TimePicker timePicker, int hour, int min) {
//                String msg = String.format("%d 시 %d 분", hour, min);
//                long revertSecond = (hour * 60 * 60) + (min * 60);
//                Log.e("jun", "revertSecond = " + revertSecond);
//                int hours = getTimeGap(revertSecond, 0, Calendar.HOUR);
//                int minutes = getTimeGap(revertSecond, 0, Calendar.MINUTE);
//
//                if(textView.getTag().equals("01")) {
//                    sleepGoTime = revertSecond;
//                } else if(textView.getTag().equals("02")) {
//                    sleepStartTime = revertSecond;
//                } else if(textView.getTag().equals("03")) {
//                    sleepEndTime = revertSecond;
//                } else if(textView.getTag().equals("04")) {
//                    sleepWentTime = revertSecond;
//                }
//
//                Log.e("jun", "hour = " + hours + " min = " + minutes);
//
//                textView.setText(msg);
//                Toast.makeText(SleepInitDiaryMainActivity.this, msg, Toast.LENGTH_SHORT).show();
//            }
//        }, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), false) {
//        };
//        dialog.setMax(19,00);
//        dialog.setMin(11,00);
//        dialog.show();
    }

    public static int getTimeGap(long before, long after, int unit) {
        long millis = before - after;
//        long millis = after - before;
        switch (unit) {
            case Calendar.HOUR:
                return (int) ((millis / (60*60)) % 24);
            case Calendar.MINUTE:
                return (int) ((millis / (60)) % 60);
            case Calendar.SECOND:
                return (int) (millis / 1000) % 60;
            default:
                return 0;
        }
    }
}
