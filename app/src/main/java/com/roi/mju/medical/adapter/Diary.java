package com.roi.mju.medical.adapter;

import java.io.Serializable;

public class Diary implements Serializable {
    String mFrontTime = "-1";
    String mRearTime = "-1";
    boolean mIsSleep;

    public Diary(String front, String rear, boolean isSleep) {
        mFrontTime = front;
        mRearTime = rear;
        mIsSleep = isSleep;
    }

    public String getFrontTime() {
        return mFrontTime;
    }

    public void setFrontTime(String frontTime) {
        this.mFrontTime = frontTime;
    }

    public String getRearTime() {
        return mRearTime;
    }

    public void setRearTime(String rearTime) {
        this.mRearTime = rearTime;
    }

    public boolean getIsSleep() {
        return mIsSleep;
    }

    public void setIsSleep(boolean IsSleep) {
        this.mIsSleep = IsSleep;
    }
}
