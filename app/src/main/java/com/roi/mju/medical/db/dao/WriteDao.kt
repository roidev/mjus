package com.roi.mju.medical.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.roi.mju.medical.db.entity.WriteDataEntity
import io.reactivex.Maybe

@Dao
interface WriteDao  : BaseDao<WriteDataEntity> {
    @Query("SELECT * FROM writeSleep WHERE id = :id")
    fun selectById(id: Int): Maybe<WriteDataEntity>

    @Query("SELECT * FROM writeSleep")
    fun selectAll(): Maybe<List<WriteDataEntity>>

    @Query("SELECT * FROM writeSleep WHERE date = :date")
    fun selectCountByDate(date : String) : Int

    @Query("SELECT * FROM writeSleep WHERE date = :date")
    fun selectByDate(date : String) : WriteDataEntity

    @Query("DELETE FROM writeSleep WHERE date = :date")
    fun deleteByDate(date : String)
}

// single = 1개가 오면 success , 안오면 error
// maybe = 행이 1개 or 0개가 오거나 update시 success -> oncomplete
// flowable = 어떤 행도 존재하지 않을 경우 onNext, onError 을 방출하지 않습니다.
// 비동기 처리시 1) AsyncTask 2) RxJava 3) Coroutine 4) JavaThread

// 코루틴(coroutine)은 루틴의 일종으로서, 협동 루틴이라 할 수 있다(코루틴의 "Co"는 with 또는 togather를 뜻한다). 상호 연계 프로그램을 일컫는다고도 표현가능하다. 루틴과 서브 루틴은 서로 비대칭적인 관계이지만,
// 코루틴들은 완전히 대칭적인, 즉 서로가 서로를 호출하는 관계이다. 코루틴들에서는 무엇이 무엇의 서브루틴인지를 구분하는 것이 불가능하다. 코루틴 A와 B가 있다고 할 때, A를 프로그래밍 할 때는 B를 A의 서브루틴으로 생각한다.
// 그러나 B를 프로그래밍할 때는 A가 B의 서브루틴이라고 생각한다. 어떠한 코루틴이 발동될 때 마다 해당 코루틴은 이전에 자신의 실행이 마지막으로 중단되었던 지점 다음의 장소에서 실행을 재개한다
//출처: https://namget.tistory.com/entry/안드로이드-ROOM-라이브러리-사용하기-코루틴?category=711279 [남갯,YTS의 개발,일상블로그]

//Coroutine에서의 dispatcher는 두가지가 있습니다
//-uiDispatcher : 안드로이드 UI스레드에서 실행합니다. (Dispatchers.Main)
//-bgDisPatcher  BG 스레드에서 실행합니다. (Dispatchers.IO)


