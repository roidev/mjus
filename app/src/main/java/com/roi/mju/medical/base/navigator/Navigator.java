package com.roi.mju.medical.base.navigator;

/**
 * Created by Roise on 2018. 9. 8..
 */

public class Navigator {
    private static INavigator sNavigator;

    public static void initialize(INavigator navigator) {
        sNavigator = navigator;
    }

    public static INavigator get() {
        return sNavigator;
    }
}
