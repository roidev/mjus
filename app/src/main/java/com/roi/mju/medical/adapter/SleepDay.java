package com.roi.mju.medical.adapter;

import java.io.Serializable;

public class SleepDay implements Serializable {
    String sleepTime = "-1";  // 보여주는 시간
    String sleepType= "-1";    // 색칠할 시간 타입
    String checkTime = "-1";    // 실제 저장된 타임

    public SleepDay(String sleepTime, String sleepType, String checkTime) {
        this.sleepTime = sleepTime;
        this.sleepType = sleepType;
        this.checkTime = checkTime;
    }

    public String getSleepTime() {
        return sleepTime;
    }

    public void setSleepTime(String sleepTime) {
        this.sleepTime = sleepTime;
    }

    public String getSleepType() {
        return sleepType;
    }

    public void setSleepType(String sleepType) {
        this.sleepType = sleepType;
    }

    public String getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(String checkTime) {
        this.checkTime = checkTime;
    }
}
