package com.roi.mju.medical.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.roi.mju.medical.R;

public class DiaryViewHolder extends RecyclerView.ViewHolder {
    public ImageView sleepTouch;
    public TextView frontTime;
    public TextView rearTime;
    public TextView sleepType;

    public DiaryViewHolder(Context context, View itemView) {
        super(itemView);

        sleepTouch = itemView.findViewById(R.id.sleepTouch);
        frontTime = itemView.findViewById(R.id.time_front);
        rearTime = itemView.findViewById(R.id.time_rear);
        sleepType = itemView.findViewById(R.id.sleep_type);
    }
}
