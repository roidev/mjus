package com.roi.mju.medical.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.DividerItemDecoration
import com.roi.mju.medical.R
import com.roi.mju.medical.adapter.SleepDay
import com.roi.mju.medical.adapter.SleepDayAdapter
import com.roi.mju.medical.db.SleepDatabase
import com.roi.mju.medical.util.AppRunExecutors
import com.roi.mju.medical.util.SleepUtil
import kotlinx.android.synthetic.main.activity_calender_main.*
import java.text.SimpleDateFormat
import java.util.*

class SleepDayActivity : AppCompatActivity() {
    lateinit var mSleepDayAdapter: SleepDayAdapter
    private val mOnItemClickListener: View.OnClickListener? = null

    internal var mSleepReplaceDate = ""
    internal var mSleepCurrentDate = ""

    internal val timeItems = arrayOf(
        "19:00 ~ 20:00",
        "20:00 ~ 21:00",
        "21:00 ~ 22:00",
        "22:00 ~ 23:00",
        "23:00 ~ 24:00",
        "24:00 ~ 01:00",
        "01:00 ~ 02:00",
        "02:00 ~ 03:00",
        "03:00 ~ 04:00",
        "04:00 ~ 05:00",
        "05:00 ~ 06:00",
        "06:00 ~ 07:00",
        "07:00 ~ 08:00",
        "08:00 ~ 09:00",
        "09:00 ~ 10:00",
        "10:00 ~ 11:00",

        "11:00 ~ 12:00",
        "12:00 ~ 13:00",
        "13:00 ~ 14:00",
        "14:00 ~ 15:00",
        "15:00 ~ 16:00",
        "16:00 ~ 17:00",
        "17:00 ~ 18:00",
        "18:00 ~ 19:00"
    )

    internal val arrayCheckTimesHour = arrayOf(
        19,  // 19:00   0
        20,  // 20:00   1
        21,  // 21:00   2
        22,  // 22:00   3
        23,  // 23:00   4
        24,  // 24:00   5
        1,   // 1시     6
        2,   // 2시     7
        3,  // 3시      8
        4,  // 4시      9
        5,  // 5시      10
        6,  // 6시      11
        7,  // 7시      12
        8,  // 8시      13
        9,  // 9시      14
        10, // 오전 10시  15
        11,           // 16
        12,           // 17
        13,           // 18
        14,           // 19
        15,           // 20
        16,           // 21
        17,           // 22
        18            // 23
    )

    internal val arrayCheckTimes = arrayOf(
        68400,  // 19:00
        72000,  // 20:00
        75600,  // 21:00
        79200,  // 22:00
        82800,  // 23:00
        0,  // 24:00
        3600,   // 1시 3600
        7200,   // 2시
        10800,  // 3시
        14400,  // 4시
        18000,  // 5시
        21600,  // 6시
        25200,  // 7시 25200
        28800,  // 8시
        32400,  // 9시
        36000,   // 오전 10시 3600 차이
        39600,   // 11시
        43200,   // 12시
        46800,     // 13시 오후 1시
        50400,     // 14시 오후 2시
        54000,     // 15시 오후 3시
        57600,     // 16시 오후 4시
        61200,     // 17시 오후 5시
        64800      // 18시 오후 6시
    )

    // 잠이 든 시간
    var mStartSleepTime = 0L
    var mStartSleepIndex = 0

    // 잠이 깬 시간
    var mEndSleepTime = 0L
    var mEndSleepIndex = 0

    // 잠자리에 누운 시간
    var mGoSleepTime = 0L
    var mGoSleepIndex = 0

    // 잠자리에서 나온 시간
    var mWentSleepTime = 0L
    var mWentSleepIndex = 0

    var mYear = 0L
    var mMonth = 0L
    var mDay = 0L

    var mGoHour = 0
    var mStartHour = 0
    var mEndHour = 0
    var mWentHour = 0


    lateinit var sleepPercentTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sleep_day)

        val intent = getIntent()
        mYear = intent.getLongExtra("currentYear", 0)
        mMonth = intent.getLongExtra("currentMonth", 0)
        mDay = intent.getLongExtra("currentDay", 0)

        mSleepCurrentDate = String.format("%d%d%d", mYear, mMonth, mDay)
        mSleepReplaceDate = SleepUtil.getReplaceDate(mYear, mMonth, mDay);

        Log.e("jun", "mSleepCurrentDate 날짜 = " + mSleepCurrentDate)
        Log.e("jun", "mSleepReplaceDate 날짜 = " + mSleepReplaceDate)

        if(mSleepReplaceDate == null) {
            mSleepReplaceDate = Date().toString();
        }
        val sleepDateTextView: TextView = findViewById(R.id.date) as TextView
        sleepDateTextView.setText(mSleepReplaceDate)

        sleepPercentTextView = findViewById(R.id.sleep_percent)

        initSleepDayView()
        initSettingDataView()
    }

    private fun initSleepDayView() {
        mSleepDayAdapter = SleepDayAdapter(this, mOnItemClickListener)
        sleepListView.adapter = mSleepDayAdapter
        sleepListView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        AppRunExecutors().runOnIoThread(Runnable {
            //                        LiveData<ArrayList<SleepData>> list = mSleepDatabase.sleepDao().selectAll();
//            val list = SleepDatabase.getInstance(this)!!.sleepDao().selectAll()
            var list = SleepDatabase.getInstance(this)!!.sleepDao().getSleepData(mSleepCurrentDate)

            if(list != null) {
                Log.e("jun", "SleepDay 선택된 날짜 = " + list.currentDate)
                Log.e("jun", "SleepDay 잠자리에 누운시간 = " + list.sleepGo)
                Log.e("jun", "SleepDay 언제 잠이 들었나요 = " + list.sleepingStart)
                Log.e("jun", "SleepDay 언제 잠이 깼나요 = " + list.sleepingEnd)
                Log.e("jun", "SleepDay 언제 잠자리에서 나왔나요 = " + list.sleepWent)

                mStartSleepTime = list.sleepingStart
                mEndSleepTime = list.sleepingEnd

                mGoSleepTime = list.sleepGo
                mWentSleepTime = list.sleepWent

                // 저장된 시간
                Log.e("jun", "SleepDay goHour = " + list.goHour + " startHour = " + list.startHour)
                Log.e("jun", "SleepDay endHour = " + list.endHour + " wentHour = " + list.wentHour)

                mGoHour = list.goHour
                mStartHour = list.startHour
                mEndHour = list.endHour
                mWentHour = list.wentHour
            }
        })
    }

    private fun initSettingDataView() {
        Handler().postDelayed({
            mStartSleepIndex = checkTimeIndex(mStartHour, 0)
            mEndSleepIndex = checkTimeIndex(mEndHour, 1)

//            Log.e("jun", "############initSettingDataView#######################")
//            Log.e("jun", "mStartSleepIndex = " + mStartSleepIndex)
//            Log.e("jun", "mEndSleepIndex = " + mEndSleepIndex)
//            Log.e("jun", "#################initSettingDataView##################")

            val goHours = getTimeGap(mGoSleepTime, 0, Calendar.HOUR)
            val goMin = getTimeGap(mGoSleepTime, 0, Calendar.MINUTE)
            var goTime = String.format("%d시 %d분", goHours, goMin)

            val startHours = getTimeGap(mStartSleepTime, 0, Calendar.HOUR)
            val startMin = getTimeGap(mStartSleepTime, 0, Calendar.MINUTE)
            var startTime = String.format("%d시 %d분", startHours, startMin)

            val endHours = getTimeGap(mEndSleepTime, 0, Calendar.HOUR)
            val endMin = getTimeGap(mEndSleepTime, 0, Calendar.MINUTE)
            var endTime = String.format("%d시 %d분", endHours, endMin)

            val wentHours = getTimeGap(mWentSleepTime, 0, Calendar.HOUR)
            val wentMin = getTimeGap(mWentSleepTime, 0, Calendar.MINUTE)
            var wentTime = String.format("%d시 %d분", wentHours, wentMin)

            Log.e("jun", "##########initSleepDayView#########################")
            Log.e("jun", "goTime = " + goTime)
            Log.e("jun", "startTime = " + startTime)
            Log.e("jun", "endTime = " + endTime)
            Log.e("jun", "wentTime = " + wentTime)
            Log.e("jun", "############initSleepDayView#######################")

            val dayList = ArrayList<SleepDay>()
            // 시간 입력
            for (i in 0 until timeItems.size) {
                dayList.add(SleepDay(timeItems[i], "", ""))
            }

            // 잔시간 index 입력
            // 누운 시간
            if(mStartSleepIndex != 0) {
                dayList[mStartSleepIndex-1].sleepType = "sleeping_go"
                dayList[mStartSleepIndex-1].sleepTime = goTime
            }

            // 자기 시작한 시간
            dayList[mStartSleepIndex].sleepType = "sleeping"
            dayList[mStartSleepIndex].sleepTime = startTime + " (잠든 시각)"

            // 자는 중
            if(mStartSleepIndex > mEndSleepIndex) {
                for (i in mStartSleepIndex until dayList.size) {
                    dayList[i].sleepType = "sleeping"
                }

                for (i in 0 until mEndSleepIndex) {
                    dayList[i].sleepType = "sleeping"
                }
            } else {
                for (i in mStartSleepIndex+1 until mEndSleepIndex) {
                    dayList[i].sleepType = "sleeping"
                }
            }


            // 잠 깬 시각
            dayList[mEndSleepIndex-1].sleepTime = endTime + " (잠깬 시각)"

            // 일어난 시간
            dayList[mEndSleepIndex].sleepType = "sleeping_went"
            dayList[mEndSleepIndex].sleepTime = wentTime

            mSleepDayAdapter.setList(dayList)

            // 수면 효율
            // 야간 수면 효율 = 잠든 시간부터 잠깬 시간 / 누운 시간부터 일어난 시간
            if(mStartSleepTime > -1 && mEndSleepTime > -1 && mGoSleepTime > -1 && mWentSleepTime > -1) {
//                var sleepPercent = SleepUtil.caculateSleepPercent(mStartSleepTime, mEndSleepTime, mGoSleepTime, mWentSleepTime)
                var sleepPercent = SleepUtil.caculateSleepPercent(mStartHour, mEndHour, mGoHour, mWentHour)
                Log.e("jun","day percent = " +sleepPercent)

                if(sleepPercent > 100) sleepPercent = 100

                sleepPercentTextView.text = sleepPercent.toString() + "%".toString()
            } else {
                sleepPercentTextView.text = "0%"
            }
        }, 500)
    }

    // 시간 인덱스 계
    fun checkTimeIndex(time: Int, type: Int): Int {
        var index = 0
        // 맨 처음 index
        if(type == 0) {
            for(i in 0 until arrayCheckTimesHour.size) {
                if(arrayCheckTimesHour[i] == time) {
                    index = i
                    break
                }
            }
        }

        // 맨 마지막 index
        else {
            for(i in 0 until arrayCheckTimesHour.size) {
                if(arrayCheckTimesHour[i] == time) {
                    index = i
                }
            }
        }

        return index
    }

    fun getTimeGap(before: Long, after: Long, unit: Int): Int {
        val millis = before - after
        when (unit) {
            Calendar.HOUR -> return (millis / (60 * 60) % 24).toInt()
            Calendar.MINUTE -> return (millis / 60 % 60).toInt()
            Calendar.SECOND -> return (millis / 1000).toInt() % 60
            else -> return 0
        }
    }
}
