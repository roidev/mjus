package com.roi.mju.medical.base.navigator;

/**
 * Created by Roise on 2018. 9. 8..
 */

public interface INavigator {
    void moveToSleepMain();

    // 명상
    void moveToMungsang();

    // 수면 지침
    void moveToSleepInstructions();

    // 이완 요법
    void moveToRelaxationTherapy();

    // 복식 호흡
    void moveToRelaxationSum();

    // 점진적 근육 이완
    void moveToMuscle();

    // 심상법
    void moveToSimsang();

    // 내 수면 바로 알기
    void moveToSleepMyKnow();

    // 내 수면 일기
    void moveToSleepMyDiary();

    // 내 하루 수면
    void moveToSleepDay(long mYear, long mMonth, long mDay);
//    void moveToSleepDay(String sleepDateData);

    // 바뀐 내 수면 일기
    void moveToSleeInitDiary();

    // 내 수면 찾기
    void moveToSleepMyFind();

    // 내 수면 지침 (선택한 것)
    void moveToSleepMyInstructions();

    // 병원 정보
    void moveToSleepInformation();
}
