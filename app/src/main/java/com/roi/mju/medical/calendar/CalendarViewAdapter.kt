package com.github.woochanlee.basecalendar

import android.graphics.Color
import android.opengl.Visibility
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.roi.mju.medical.R
import com.roi.mju.medical.activity.SleepDiaryMainActivity
import com.roi.mju.medical.base.navigator.Navigator
import com.roi.mju.medical.calendar.CalendarVO
import com.roi.mju.medical.db.SleepDatabase
import com.roi.mju.medical.util.AppRunExecutors
import kotlinx.android.synthetic.main.activity_sleep_my_instructions_main.*
import kotlinx.android.synthetic.main.item_schedule.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class CalendarViewAdapter(val sleepDiaryMainActivity: SleepDiaryMainActivity) : RecyclerView.Adapter<ViewHolderHelper>() {

    val baseCalendar = BaseCalendar()

    init {
        baseCalendar.initBaseCalendar {
            refreshView(it, true)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderHelper {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_schedule, parent, false)
        return ViewHolderHelper(view)
    }

    override fun getItemCount(): Int {
        return BaseCalendar.LOW_OF_CALENDAR * BaseCalendar.DAYS_OF_WEEK
    }

    override fun onBindViewHolder(holder: ViewHolderHelper, position: Int) {

        if (position % BaseCalendar.DAYS_OF_WEEK == 0) holder.tv_date.setTextColor(Color.parseColor("#ff1200")) // 주말 토요일
        else holder.tv_date.setTextColor(Color.parseColor("#000000")) // 평일
//        holder.tv_date.setTextColor(Color.parseColor("#ff1200")) // 이미 저장된 날짜

        if (position < baseCalendar.prevMonthTailOffset || position >= baseCalendar.prevMonthTailOffset + baseCalendar.currentMonthMaxDate) {
            holder.tv_date.alpha = 0.3f
        } else {
            holder.tv_date.alpha = 1f
            //        holder.tv_date.text = baseCalendar.data[position].toString()
            holder.tv_date.text = String.format("%d", baseCalendar.data[position].day)

            // 날짜에 맞는 수면 효율 셋팅
            if(baseCalendar.data[position].percent > 0) {
                var sleepPercent = String.format("%d", baseCalendar.data[position].percent)
                holder.tv_date_percent.text = sleepPercent + "%"
            }
        }

        holder.tv_layout.setOnClickListener(View.OnClickListener {
            if (position < baseCalendar.prevMonthTailOffset || position >= baseCalendar.prevMonthTailOffset + baseCalendar.currentMonthMaxDate) {

            } else {

//                var clickDate = String.format("%d%d%s", baseCalendar.calendar.get(Calendar.YEAR), (baseCalendar.calendar.get(Calendar.MONTH)+1), holder.tv_date.text)
//                Log.e("jun","clickDate = " + clickDate)
//                Log.e("jun","sleepPercent22 = " + baseCalendar.data[position].percent)

                // 수면 화면으로 이동
                if(baseCalendar.data[position].percent > 0) {
                    var mMonth = baseCalendar.calendar.get(Calendar.MONTH)+1
                    var mDay = String.format("%s", holder.tv_date.text)
                    Navigator.get().moveToSleepDay(baseCalendar.calendar.get(Calendar.YEAR).toLong(), mMonth.toLong(), mDay.toLong())

//                    Toast.makeText(sleepDiaryMainActivity, "click day = " + baseCalendar.calendar.get(Calendar.YEAR) + (baseCalendar.calendar.get(Calendar.MONTH)+1) + mDay, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    fun changeToPrevMonth() {
        baseCalendar.changeToPrevMonth {
            refreshView(it, false)
        }
    }

    fun changeToNextMonth() {
        baseCalendar.changeToNextMonth {
            refreshView(it, false)
        }
    }

    private fun refreshView(calendar: Calendar, isFirst: Boolean) {
        sleepDiaryMainActivity.refreshCurrentMonth(calendar)
        if(!isFirst) {
            sleepDiaryMainActivity.makeSleepPercent()
            notifyDataSetChanged()
        }
    }

    fun refreshCalendar() {
        for(i in baseCalendar.data.indices) {
            if(baseCalendar.data.get(i).percent > 0) {
                Log.e("jun","refreshCalendar = " + baseCalendar.data.get(i).day + " per = " + baseCalendar.data.get(i).percent)
            }
        }

        notifyDataSetChanged()
    }
}