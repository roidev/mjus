package com.roi.mju.medical.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import com.github.woochanlee.basecalendar.BaseCalendar
import com.github.woochanlee.basecalendar.CalendarViewAdapter
import com.roi.mju.medical.R
import com.roi.mju.medical.adapter.Diary
import com.roi.mju.medical.adapter.SleepDiaryAdapter
import com.roi.mju.medical.db.SleepDatabase
import com.roi.mju.medical.db.dao.SleepData
import com.roi.mju.medical.util.AppRunExecutors
import kotlinx.android.synthetic.main.activity_calender_main.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * 내 수면 일기 ..젤 어려운 곳
 */
class SleepDiaryMainActivity : AppCompatActivity() {
    lateinit var mSleepDiaryAdapter: SleepDiaryAdapter
    private val mOnItemClickListener: View.OnClickListener? = null

    lateinit var scheduleCalendarViewAdapter: CalendarViewAdapter
    private var mSleepDatabase: SleepDatabase? = null
    var sleepDataList: List<SleepData>? = null
    var sleepMonth: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calender_main)

//        initSleepDayView()
        initView()
        getMonthSleepData()
    }

    private fun getMonthSleepData() {
        AppRunExecutors().runOnIoThread(Runnable {
            //                        LiveData<ArrayList<SleepData>> list = mSleepDatabase.sleepDao().selectAll();
            mSleepDatabase = SleepDatabase.getInstance(this)
            sleepDataList = mSleepDatabase?.sleepDao()?.getAllSleepData()

     /*       for (i in sleepDataList!!.indices) {
                Log.e("jun", "########## Diary getMonthSleepData  #########################")
                Log.e("jun", "선택된 날짜 = " + sleepDataList?.get(i)?.currentDate)
                Log.e("jun", "잠자리에 누운시간 = " + sleepDataList?.get(i)?.sleepGo)
                Log.e("jun", "언제 잠이 들었나요 = " + sleepDataList?.get(i)?.sleepingStart)
                Log.e("jun", "언제 잠이 깼나요 = " + sleepDataList?.get(i)?.sleepingEnd)
                Log.e("jun", "언제 잠자리에서 나왔나요 = " + sleepDataList?.get(i)?.sleepWent)
                Log.e("jun", "수면 효율 = " + sleepDataList?.get(i)?.sleepPercent)
            }*/
        })

        android.os.Handler().postDelayed({ makeSleepPercent() }, 500)
    }

    // 데이터 넣어줌..
    fun makeSleepPercent() {
        var calendar = Calendar.getInstance()

        var calendarList = scheduleCalendarViewAdapter.baseCalendar.data

        // 날짜 넣고 있으면 갱신
        for(i in calendarList.indices) {
            var day = sleepMonth + calendarList.get(i).day
//            Log.e("jun","makeSleepPercent day = " + day)
            for (j in sleepDataList!!.indices) {
                if(day.equals(sleepDataList!!.get(j).currentDate)) {
//                    Log.e("jun","makeSleepPercent currentDate = " + sleepDataList!!.get(j).currentDate)
                    calendarList.get(i).percent = sleepDataList!!.get(j).sleepPercent
                }
            }
        }

        scheduleCalendarViewAdapter.refreshCalendar()
    }

    fun findSleepPercent(date: String): Long {
        var percent = 0L
        Handler().post({
            var mSleepDatabase = SleepDatabase.getInstance(this)
            var sleepData = mSleepDatabase?.sleepDao()?.getSleepData(date)

            if(sleepData != null) percent = sleepData.sleepPercent
//            Log.e("jun","findSleepPercent = " + percent)
        })
        return percent
    }

    fun initView() {

        scheduleCalendarViewAdapter = CalendarViewAdapter(this)

        rv_schedule.layoutManager = GridLayoutManager(this, BaseCalendar.DAYS_OF_WEEK)
        rv_schedule.adapter = scheduleCalendarViewAdapter
        rv_schedule.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL))
        rv_schedule.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))

        tv_prev_month.setOnClickListener {
            scheduleCalendarViewAdapter.changeToPrevMonth()
        }

        tv_next_month.setOnClickListener {
            scheduleCalendarViewAdapter.changeToNextMonth()
        }
    }

    fun refreshCurrentMonth(calendar: Calendar) {
        val sdf = SimpleDateFormat("yyyy MM", Locale.KOREAN)
        tv_current_month.text = sdf.format(calendar.time)

        val tempSdf = SimpleDateFormat("yyyyM", Locale.KOREAN)
        sleepMonth = tempSdf.format(calendar.time)
    }

    private fun initSleepDayView() {
        mSleepDiaryAdapter = SleepDiaryAdapter(this, mOnItemClickListener)
        sleepListView.adapter = mSleepDiaryAdapter

        val diaryArrayList = ArrayList<Diary>()
        diaryArrayList.add(Diary("19", "20", false))
        diaryArrayList.add(Diary("20", "21", false))
        diaryArrayList.add(Diary("21", "22", false))
        diaryArrayList.add(Diary("22", "23", false))
        diaryArrayList.add(Diary("23", "24", false))
        diaryArrayList.add(Diary("24", "01", false))
        diaryArrayList.add(Diary("01", "02", false))
        diaryArrayList.add(Diary("02", "03", false))
        diaryArrayList.add(Diary("03", "04", false))
        diaryArrayList.add(Diary("04", "05", false))
        diaryArrayList.add(Diary("05", "06", false))
        diaryArrayList.add(Diary("06", "07", false))
        diaryArrayList.add(Diary("07", "08", false))
        diaryArrayList.add(Diary("08", "09", false))
        diaryArrayList.add(Diary("09", "10", false))
        diaryArrayList.add(Diary("10", "11", false))

        mSleepDiaryAdapter.setList(diaryArrayList)
    }
}
