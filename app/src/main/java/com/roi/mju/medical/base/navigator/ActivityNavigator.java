package com.roi.mju.medical.base.navigator;

import android.content.Context;
import android.content.Intent;
import com.roi.mju.medical.SleepMainActivity;
import com.roi.mju.medical.activity.*;
import com.roi.mju.medical.mungsang.SleepMungsangActivity;
import com.roi.mju.medical.muscle.SleepMuscleActivity;
import com.roi.mju.medical.relaxation.RelaxationSumActivity;
import com.roi.mju.medical.simsang.SimsangFragment;
import com.roi.mju.medical.simsang.SimsangMainActivity;

/**
 * Created by Roise on 2019.06.12
 */

public class ActivityNavigator implements INavigator {
    private Context mContext;

    public ActivityNavigator(Context context) {
        this.mContext = context;
    }

    @Override
    public void moveToSleepMain() {
        Intent intent = new Intent(mContext, SleepMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    // 명상
    @Override
    public void moveToMungsang() {
        Intent intent = new Intent(mContext, SleepMungsangActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    // 수면 지침
    @Override
    public void moveToSleepInstructions() {
        Intent intent = new Intent(mContext, SleepInstructionsMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    // 이완 요법
    @Override
    public void moveToRelaxationTherapy() {
        Intent intent = new Intent(mContext, SleepRelaxationTherapyMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    // 복식 호흡
    @Override
    public void moveToRelaxationSum() {
        Intent intent = new Intent(mContext, RelaxationSumActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    // 점진적 근육 이완
    @Override
    public void moveToMuscle() {
        Intent intent = new Intent(mContext, SleepMuscleActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    // 심상법
    @Override
    public void moveToSimsang() {
        Intent intent = new Intent(mContext, SimsangMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    // 내 수면 바로 알기
    @Override
    public void moveToSleepMyKnow() {
        Intent intent = new Intent(mContext, SleepMuscleActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    // 내 수면 일기
    @Override
    public void moveToSleepMyDiary() {
//        Intent intent = new Intent(mContext, SleepInitDiaryMainActivity.class);
        Intent intent = new Intent(mContext, SleepDiaryMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    // 내 하루 수면
    @Override
    public void moveToSleepDay(long mYear, long mMonth, long mDay) {
        Intent intent = new Intent(mContext, SleepDayActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("currentYear", mYear);
        intent.putExtra("currentMonth", mMonth);
        intent.putExtra("currentDay", mDay);
        mContext.startActivity(intent);
    }

    // 내 수면 일기
    @Override
    public void moveToSleeInitDiary() {
        Intent intent = new Intent(mContext, SleepInitDiaryMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    // 내 수면 찾기
    @Override
    public void moveToSleepMyFind() {
        Intent intent = new Intent(mContext, SleepFindMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    // 내 수면 지침 (선택한 것)
    @Override
    public void moveToSleepMyInstructions() {
        Intent intent = new Intent(mContext, SleepMyInstructionsMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }

    @Override
    public void moveToSleepInformation() {
        Intent intent = new Intent(mContext, SleepInfoMainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(intent);
    }
}
