package com.roi.mju.medical.muscle

import android.content.Context
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.roi.mju.medical.R
import com.roi.mju.medical.relaxation.RelaxationFirstFragment
import com.roi.mju.medical.util.AppRunExecutors
import me.relex.circleindicator.CircleIndicator

/**
 * 점진적 근육 이완
 */
class SleepMuscleActivity : AppCompatActivity() {
    var adapterViewPager: FragmentPagerAdapter? = null
    var mediaPlayer: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sleep_my_main)

        val vpPager = findViewById(R.id.vpPager) as ViewPager
        adapterViewPager = MyPagerAdapter(supportFragmentManager, this)
        vpPager.adapter = adapterViewPager

        val indicator = findViewById(R.id.indicator) as CircleIndicator
        indicator.setViewPager(vpPager)

//        mediaPlayer = MediaPlayer.create(this, R.raw.muscle)
//        AppRunExecutors().runOnIoThread(Runnable {
//            Log.e("jun", "mediaPlayer start")
//            mediaPlayer!!.start()
//        })
    }

    override fun onDestroy() {
        super.onDestroy()
//        mediaPlayer!!.release()
//        mediaPlayer = null
    }

    class MyPagerAdapter(fragmentManager: FragmentManager, context: Context) : FragmentPagerAdapter(fragmentManager) {
        var mContext = context

        override fun getCount(): Int {
            return NUM_ITEMS
        }

        // Returns the fragment to display for that page
        override fun getItem(position: Int): Fragment {
            when (position) {
                0 -> return RelaxationFirstFragment.newInstance(0, mContext.resources.getString(R.string.relax_muscle_main_title))
                1 -> return RelaxationFirstFragment.newInstance(1, mContext.resources.getString(R.string.relax_muscle_main_title_01))
                2 -> return RelaxationFirstFragment.newInstance(1, mContext.resources.getString(R.string.relax_muscle_main_title_02))
                3 -> return RelaxationFirstFragment.newInstance(1, mContext.resources.getString(R.string.relax_muscle_main_title_03))
                4 -> return RelaxationFirstFragment.newInstance(1, mContext.resources.getString(R.string.relax_muscle_main_title_04))
                5 -> return RelaxationFirstFragment.newInstance(1, mContext.resources.getString(R.string.relax_muscle_main_title_05))
                else -> return RelaxationFirstFragment.newInstance(0, "Page # 1")
            }
        }

        // Returns the page title for the top indicator
        override fun getPageTitle(position: Int): CharSequence? {
            return "Page $position"
        }

        companion object {
            private val NUM_ITEMS = 6
        }
    }
}
