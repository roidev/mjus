package com.roi.mju.medical.find

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.roi.mju.medical.R

class FindFirstFragment : Fragment() {

    companion object {
        fun newInstance() = FindFirstFragment()
    }

    private lateinit var viewModel: FindFirstViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.find_first_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(FindFirstViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
