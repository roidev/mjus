package com.roi.mju.medical.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.roi.mju.medical.R;

import java.util.ArrayList;

public class SleepDayAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_SLEEP = 0;

    private int timeIndex = -1;
    private Context mContext;
    private ArrayList<SleepDay> mSleepDayList = new ArrayList();
    private View.OnClickListener mOnItemClickListener;

    public SleepDayAdapter(Context context, View.OnClickListener onItemClickListener) {
        this.mContext = context;
        this.mOnItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEW_TYPE_SLEEP:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_sleep_day, parent, false);
                return new SleepDayViewHolder(mContext, view);
            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_sleep_day, parent, false);
                return new SleepDayViewHolder(mContext, view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int itemViewType = getItemViewType(position);
        final SleepDayViewHolder sleepDayViewHolder = (SleepDayViewHolder) holder;
        SleepDay item = mSleepDayList.get(position);

//        if (item.sleepType.equals("sleeping_go")) {
//            mSleepDayList.get(position-1).sleepType = "black";
//        } else if (item.sleepType.equals("sleeping_went")) {
//            mSleepDayList.get(position+1).sleepType = "black";
//        }

        sleepDayViewHolder.sleepTime.setText(item.getSleepTime());
        setItemColor(sleepDayViewHolder, item.getSleepType());
    }

    @Override
    public int getItemCount() {
        return mSleepDayList == null? 0 : mSleepDayList.size();
    }

    @Override
    public int getItemViewType(int position) {
//        return super.getItemViewType(position);
        return VIEW_TYPE_SLEEP;
    }

    public void setList(ArrayList<SleepDay> list) {
        mSleepDayList = list;
        notifyDataSetChanged();
    }

    public void setItemColor(final SleepDayViewHolder viewHolder, String sleepType) {
        if(sleepType.equals("sleeping")) {
            viewHolder.sleepType.setBackgroundResource(R.color.item_wake_background);
            viewHolder.sleepType.setVisibility(View.VISIBLE);
            viewHolder.sleepTypeText.setVisibility(View.GONE);
        } else if (sleepType.equals("sleeping_go")) {
            viewHolder.sleepType.setVisibility(View.GONE);
            viewHolder.sleepTime.setTextColor(mContext.getResources().getColor(R.color.colorBlack));

            viewHolder.sleepTypeText.setVisibility(View.VISIBLE);
            viewHolder.sleepTypeText.setTextColor(mContext.getResources().getColor(R.color.colorBlack));
            viewHolder.sleepTypeText.setText("누운 시간");
        } else if (sleepType.equals("sleeping_went")) {
            viewHolder.sleepType.setVisibility(View.GONE);
            viewHolder.sleepTime.setTextColor(mContext.getResources().getColor(R.color.colorBlack));

            viewHolder.sleepTypeText.setVisibility(View.VISIBLE);
            viewHolder.sleepTypeText.setTextColor(mContext.getResources().getColor(R.color.colorBlack));
            viewHolder.sleepTypeText.setText("일어난 시간");
        } else if (sleepType.equals("black")) {
            viewHolder.sleepTime.setTextColor(mContext.getResources().getColor(R.color.colorBlack));
        }
        else {
            viewHolder.sleepType.setBackgroundResource(R.color.colorWhite);
            viewHolder.sleepType.setVisibility(View.VISIBLE);
            viewHolder.sleepTypeText.setVisibility(View.GONE);
        }
    }
}


