package com.roi.mju.medical.instructions;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import com.roi.mju.medical.R;
import com.roi.mju.medical.preference.PreferenceManager;

public class InstructionsFifthFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private int mParam1;
    private String mParam2;

    private CheckBox ok;
    private CheckBox no;

    public InstructionsFifthFragment() {
        // Required empty public constructor
    }

    public static InstructionsFifthFragment newInstance(int param1, String param2) {
        InstructionsFifthFragment fragment = new InstructionsFifthFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getInt(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.sleep_common_fragment, container, false);
        TextView text = view.findViewById(R.id.text1);
        text.setText(mParam2);

        view.findViewById(R.id.check_layout).setVisibility(View.VISIBLE);
        ok = view.findViewById(R.id.check_ok);
        no = view.findViewById(R.id.check_no);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox)view).isChecked()) {
                    // TODO : CheckBox is checked.
                    PreferenceManager.getInstructions().setBoolean("in05", false);
                    no.setChecked(false);
                } else {
                    // TODO : CheckBox is unchecked.
                    PreferenceManager.getInstructions().setBoolean("in05", true);
                    no.setChecked(true);
                }
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CheckBox)view).isChecked()) {
                    // TODO : CheckBox is checked.
                    PreferenceManager.getInstructions().setBoolean("in05", true);
                    ok.setChecked(false);
                } else {
                    // TODO : CheckBox is unchecked.
                    PreferenceManager.getInstructions().setBoolean("in05", false);
                    ok.setChecked(true);
                }
            }
        });
        if(PreferenceManager.getInstructions().getBoolean("in05", true)) {
            ok.setChecked(false);
            no.setChecked(true);
        } else {
            ok.setChecked(true);
            no.setChecked(false);
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
