package com.roi.mju.medical.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.roi.mju.medical.R
import com.roi.mju.medical.preference.PreferenceManager
import kotlinx.android.synthetic.main.activity_sleep_my_instructions_main.*

/**
 * 내 수면 지침 (선택된 수면 지침)
 */
class SleepMyInstructionsMainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sleep_my_instructions_main)

        if(PreferenceManager.getInstructions().getBoolean("in02", true)) {
            text2.visibility = View.VISIBLE
        } else {
            text2.visibility = View.GONE
        }

        if(PreferenceManager.getInstructions().getBoolean("in03", true)) {
            text3.visibility = View.VISIBLE
        } else {
            text3.visibility = View.GONE
        }

        if(PreferenceManager.getInstructions().getBoolean("in04", true)) {
            text4.visibility = View.VISIBLE
        } else {
            text4.visibility = View.GONE
        }

        if(PreferenceManager.getInstructions().getBoolean("in05", true)) {
            text5.visibility = View.VISIBLE
        } else {
            text5.visibility = View.GONE
        }

        if(PreferenceManager.getInstructions().getBoolean("in06", true)) {
            text6.visibility = View.VISIBLE
        } else {
            text6.visibility = View.GONE
        }

        if(PreferenceManager.getInstructions().getBoolean("in07", true)) {
            text7.visibility = View.VISIBLE
        } else {
            text7.visibility = View.GONE
        }

        if(PreferenceManager.getInstructions().getBoolean("in08", true)) {
            text8.visibility = View.VISIBLE
        } else {
            text8.visibility = View.GONE
        }
    }
}
