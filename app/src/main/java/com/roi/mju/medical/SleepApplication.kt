package com.roi.mju.medical

import android.app.Application
import com.roi.mju.medical.base.navigator.ActivityNavigator
import com.roi.mju.medical.base.navigator.Navigator
import com.roi.mju.medical.db.SleepDatabase

class SleepApplication : Application() {

    companion object {
        protected lateinit var sSingleton: SleepApplication
        fun getInstance(): SleepApplication {
            return sSingleton
        }
    }

    override fun onCreate() {
        super.onCreate()
        sSingleton = this;
        initializeNavigator()
    }

    private fun initializeNavigator() {
        val navigator = ActivityNavigator(this)
        Navigator.initialize(navigator)
    }
}