package com.roi.mju.medical.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.roi.mju.medical.R;

public class SleepDayViewHolder extends RecyclerView.ViewHolder {
    public TextView sleepTime;
    public ImageView sleepType;
    public TextView sleepTypeText;

    public SleepDayViewHolder(Context context, View itemView) {
        super(itemView);

        sleepTime = itemView.findViewById(R.id.sleepTime);
        sleepType = itemView.findViewById(R.id.sleepType);
        sleepTypeText = itemView.findViewById(R.id.sleepTypeText);
    }
}
