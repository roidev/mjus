package com.roi.mju.medical.db.dao;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = SleepData.TABLE_NAME_SLEEP_DATA)
public class SleepData {
    public static final String TABLE_NAME_SLEEP_DATA = "SleepData";
    public static final String COLUMN_CODE = "SleepCode";

    private long year;  // 년
    private long month; // 월
    private long day;   // 일

    private String currentDate;     // 저장한 날자
    private long sleepGo;           // 누운 시간
    private long sleepingStart;     // 잠든 시간
    private long sleepingEnd;       // 일어난 시간
    private long sleepWent;         // 일어나서 나온 시간
    private long sleepPercent;      // 수면 효율

    private int goHour;
    private int startHour;
    private int endHour;
    private int wentHour;

    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = COLUMN_CODE, index = true)
    private long id = 0;

    public SleepData() {
    }

    public SleepData(long year, long month, long day, String date, long go, long start, long end, long went, long percent, int goHour, int startHour, int endHour, int wentHour) {
        this.year = year;
        this.month = month;
        this.day = day;

        this.currentDate = date;
        this.sleepGo = go;
        this.sleepingStart = start;
        this.sleepingEnd = end;
        this.sleepWent = went;

        this.sleepPercent = percent;

        this.goHour = goHour;
        this.startHour = startHour;
        this.endHour = endHour;
        this.wentHour = wentHour;
    }

    public int getGoHour() {
        return goHour;
    }

    public void setGoHour(int goHour) {
        this.goHour = goHour;
    }

    public int getStartHour() {
        return startHour;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public int getEndHour() {
        return endHour;
    }

    public void setEndHour(int endHour) {
        this.endHour = endHour;
    }

    public int getWentHour() {
        return wentHour;
    }

    public void setWentHour(int wentHour) {
        this.wentHour = wentHour;
    }

    public long getSleepPercent() {
        return sleepPercent;
    }

    public void setSleepPercent(long sleepPercent) {
        this.sleepPercent = sleepPercent;
    }

    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }

    public long getMonth() {
        return month;
    }

    public void setMonth(long month) {
        this.month = month;
    }

    public long getDay() {
        return day;
    }

    public void setDay(long day) {
        this.day = day;
    }

    public long getId() {
        return id;
    }

    public void setId(long code) {
        this.id = code;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void setCurrentDate(String currentDate) {
        this.currentDate = currentDate;
    }

    public long getSleepGo() {
        return sleepGo;
    }

    public void setSleepGo(long sleepGo) {
        this.sleepGo = sleepGo;
    }

    public long getSleepingStart() {
        return sleepingStart;
    }

    public void setSleepingStart(long sleepingStart) {
        this.sleepingStart = sleepingStart;
    }

    public long getSleepingEnd() {
        return sleepingEnd;
    }

    public void setSleepingEnd(long sleepingEnd) {
        this.sleepingEnd = sleepingEnd;
    }

    public long getSleepWent() {
        return sleepWent;
    }

    public void setSleepWent(long sleepWent) {
        this.sleepWent = sleepWent;
    }
}
