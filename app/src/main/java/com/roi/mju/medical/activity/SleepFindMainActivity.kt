package com.roi.mju.medical.activity

import android.content.Context
import android.content.res.Resources
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.roi.mju.medical.R
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.viewpager.widget.ViewPager
import com.roi.mju.medical.find.*
import me.relex.circleindicator.CircleIndicator


class SleepFindMainActivity : AppCompatActivity() {
//    private var mBinding: ActivitySleepFindMainBinding? = null
    var adapterViewPager: FragmentPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sleep_find_main)
//        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_sleep_find_main)

        val vpPager = findViewById(R.id.vpPager) as ViewPager
        adapterViewPager = MyPagerAdapter(supportFragmentManager, this)
        vpPager.adapter = adapterViewPager

        val indicator = findViewById(R.id.indicator) as CircleIndicator
        indicator.setViewPager(vpPager)
    }

    class MyPagerAdapter(fragmentManager: FragmentManager, context: Context) : FragmentPagerAdapter(fragmentManager) {
        var mContext = context

        override fun getCount(): Int {
            return NUM_ITEMS
        }

        // Returns the fragment to display for that page
        override fun getItem(position: Int): Fragment {
            when (position) {
                0 -> return FindMainFragment.newInstance(0, mContext.resources.getString(R.string.find_sleep_page_01))
                1 -> return FindMainFragment.newInstance(1, mContext.resources.getString(R.string.find_sleep_page_02))
                2 -> return FindMainFragment.newInstance(2, mContext.resources.getString(R.string.find_sleep_page_03))
                3 -> return FindMainFragment.newInstance(3, mContext.resources.getString(R.string.find_sleep_page_04))
                4 -> return FindMainFragment.newInstance(4, mContext.resources.getString(R.string.find_sleep_page_05))
                else -> return FindMainFragment.newInstance(0, "Page # 1")
            }
        }

        // Returns the page title for the top indicator
        override fun getPageTitle(position: Int): CharSequence? {
            return "Page $position"
        }

        companion object {
            private val NUM_ITEMS = 5
        }
    }

}
