package com.github.woochanlee.basecalendar

import android.content.Context
import android.util.Log
import com.roi.mju.medical.SleepApplication
import com.roi.mju.medical.calendar.CalendarVO
import com.roi.mju.medical.db.SleepDatabase
import com.roi.mju.medical.util.AppRunExecutors
import java.text.SimpleDateFormat
import java.util.*
import java.util.logging.Handler
import kotlin.collections.ArrayList

class BaseCalendar {

    companion object {
        const val DAYS_OF_WEEK = 7
        const val LOW_OF_CALENDAR = 6
    }

    val calendar = Calendar.getInstance()

    var prevMonthTailOffset = 0
    var nextMonthHeadOffset = 0
    var currentMonthMaxDate = 0

    var data = arrayListOf<CalendarVO>()
    var mContext = null

    init {
        calendar.time = Date()
    }

    /**
     * Init calendar.
     */
    fun initBaseCalendar(refreshCallback: (Calendar) -> Unit) {
        makeMonthDate(refreshCallback)
    }

    /**
     * Change to prev month.
     */
    fun changeToPrevMonth(refreshCallback: (Calendar) -> Unit) {
        if(calendar.get(Calendar.MONTH) == 0){
            calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 1)
            calendar.set(Calendar.MONTH, Calendar.DECEMBER)
        }else {
            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1)
        }
        makeMonthDate(refreshCallback)
    }

    /**
     * Change to next month.
     */
    fun changeToNextMonth(refreshCallback: (Calendar) -> Unit) {
        if(calendar.get(Calendar.MONTH) == Calendar.DECEMBER){
            calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) + 1)
            calendar.set(Calendar.MONTH, 0)
        }else {
            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) + 1)
        }
        makeMonthDate(refreshCallback)
    }

    /**
     * make month date.
     */
    private fun makeMonthDate(refreshCallback: (Calendar) -> Unit) {

        data.clear()

        calendar.set(Calendar.DATE, 1)

        currentMonthMaxDate = calendar.getActualMaximum(Calendar.DAY_OF_MONTH)

        prevMonthTailOffset = calendar.get(Calendar.DAY_OF_WEEK) - 1

        makePrevMonthTail(calendar.clone() as Calendar)
        makeCurrentMonth(calendar)

        nextMonthHeadOffset = LOW_OF_CALENDAR * DAYS_OF_WEEK - (prevMonthTailOffset + currentMonthMaxDate)
        makeNextMonthHead()

//        makeSleepPercent()

        refreshCallback(calendar)
    }

    /**
     * Generate data for the last month displayed before the first day of the current calendar.
     */
    private fun makePrevMonthTail(calendar: Calendar) {
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1)
        val maxDate = calendar.getActualMaximum(Calendar.DATE)
        var maxOffsetDate = maxDate - prevMonthTailOffset

        for (i in 1..prevMonthTailOffset) data.add(CalendarVO(++maxOffsetDate, 0L))
    }

    /**
     * Generate data for the current calendar.
     */
    private fun makeCurrentMonth(calendar: Calendar) {
        for (i in 1..calendar.getActualMaximum(Calendar.DATE)) {
            data.add(CalendarVO(i, 0L))
        }
    }

    /**
     * Generate data for the next month displayed before the last day of the current calendar.
     */
    private fun makeNextMonthHead() {
        var date = 1

        for (i in 1..nextMonthHeadOffset) data.add(CalendarVO(date++, 0L))
    }

    fun getCalendarData(): ArrayList<CalendarVO> {
        for(i in data.indices) {
            Log.e("jun","getCalendarData day = " + data.get(i).day)
        }
        return data;
    }

    private fun makeSleepPercent() {
        val month = SimpleDateFormat("yyyyM", Locale.KOREAN)
        var findMonth = month.format(calendar.time)
        Log.e("jun","makeSleepPercent month = " + findMonth)

        // 날짜 넣고 있으면 갱신
        for(i in data.indices) {
            var calPercent = findSleepPercent(findMonth + data.get(i).day)
            Log.e("jun","##calPercent = " + calPercent)
            if(calPercent > 0) {
                data.get(i).percent = findSleepPercent(findMonth + data.get(i).day)
                Log.e("jun","##calPercent day = " + findMonth+data.get(i).day + " percent = " + data.get(i).percent)
            }
        }
    }

    fun findSleepPercent(date: String): Long {
        var percent = 0L
        AppRunExecutors().runOnIoThread(Runnable {
            var mSleepDatabase = SleepDatabase.getInstance(SleepApplication.getInstance().applicationContext)
            var sleepData = mSleepDatabase?.sleepDao()?.getSleepData(date)
            percent = if(sleepData?.sleepPercent == null) {
                0
            } else {
                Log.e("jun","##findSleepPercent = " + sleepData!!.sleepPercent)
                sleepData!!.sleepPercent
            }

        })

        return percent
    }
}