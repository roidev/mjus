package com.roi.mju.medical.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.roi.mju.medical.R;

import java.util.ArrayList;

public class SleepDiaryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_SLEEP = 0;
//    private static final int VIEW_TYPE_FOLLOWING = 1;
//    private static final int VIEW_TYPE_STAR = 2;
//    private static final int HEADER_TYPE = 2;

    final String[] items = new String[]{"19:00 ~ 20:00", "20:00 ~ 21:00", "21:00 ~ 22:00", "22:00 ~ 23:00", "23:00 ~ 24:00",
            "24:00 ~ 01:00", "01:00 ~ 02:00", "02:00 ~ 03:00", "03:00 ~ 04:00", "04:00 ~ 05:00", "05:00 ~ 06:00", "06:00 ~ 07:00", "07:00 ~ 08:00",
            "08:00 ~ 09:00", "09:00 ~ 10:00", "10:00 ~ 11:00"};
    boolean[] checked = {false, false, false, false, false, false, false, false, false, false,
            false, false, false, false, false, false,};

    private int timeIndex = -1;
    private Context mContext;
    private ArrayList<Diary> mDiaryList = new ArrayList();
    private  View.OnClickListener mOnItemClickListener;

    public SleepDiaryAdapter(Context context,  View.OnClickListener onItemClickListener) {
        this.mContext = context;
        this.mOnItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEW_TYPE_SLEEP:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_sleep_diary, parent, false);
                return new DiaryViewHolder(mContext, view);
            default:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_sleep_diary, parent, false);
                return new DiaryViewHolder(mContext, view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int itemViewType = getItemViewType(position);
        final DiaryViewHolder diaryViewHolder = (DiaryViewHolder) holder;
        Diary item = mDiaryList.get(position);

        diaryViewHolder.frontTime.setText(item.getFrontTime());
        diaryViewHolder.rearTime.setText(item.getRearTime());
        if(item.getRearTime().equals("-1")) {
            diaryViewHolder.rearTime.setVisibility(View.GONE);
        } else {
            diaryViewHolder.rearTime.setVisibility(View.VISIBLE);
        }

        if(item.getIsSleep()){
            diaryViewHolder.sleepTouch.setBackgroundResource(R.color.item_coffee_background);
        } else {
            diaryViewHolder.sleepTouch.setBackgroundResource(R.color.item_background);
        }

//        diaryViewHolder.sleepType.setText(item.getSleepType());
//        diaryViewHolder.sleepTouch.setOnClickListener(mOnItemClickListener);

        diaryViewHolder.sleepTouch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChoiceDialog(mContext, diaryViewHolder);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDiaryList == null? 0 : mDiaryList.size();
    }

    @Override
    public int getItemViewType(int position) {
//        return super.getItemViewType(position);
        return VIEW_TYPE_SLEEP;
    }

    public void setList(ArrayList<Diary> list) {
        mDiaryList = list;
        notifyDataSetChanged();
    }

    public void showChoiceDialog(Context context, final DiaryViewHolder viewHolder) {
        final int[] selectedIndex = {0};

        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setTitle("잠든 시간을 선택해주세요").setMultiChoiceItems(items, checked, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int index, boolean isChecked) {
                checked[index] = isChecked;
            }
        }).setPositiveButton("확인", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int index) {
                String str = "선택된 값은 : ";
                for(int i=0; i<checked.length; i++) {
                    if(checked[i]) {
                        str = str + items[i] + ", ";
                    }
                }

                setItemSleepColor(viewHolder);
                notifyDataSetChanged();
                Log.e("jun","result = " + str);
            }
        }).setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                Log.e("jun","onDismiss");
                clearList();
            }
        }).create().show();


//        dialog.setTitle("잠든 시간을 선택해주세요").setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                selectedIndex[0] = i;
//            }
//        }).setPositiveButton("확인", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
////                Toast.makeText(mContext, "선택된 item = " + items[selectedIndex[0]], Toast.LENGTH_SHORT).show();
//                setItemColor(viewHolder, selectedIndex[0]);
//            }
//        }).create().show();
    }

    public void clearList() {
        for(int i=0; i<checked.length; i++) {
            checked[i] = false;
        }

        for(int i=0; i<mDiaryList.size(); i++) {
            mDiaryList.get(i).setIsSleep(false);
        }
    }

    public void setItemSleepColor(final DiaryViewHolder viewHolder) {
        for(int i=0; i<mDiaryList.size(); i++) {
           mDiaryList.get(i).setIsSleep(checked[i]);
        }

        for(int i=0; i<mDiaryList.size(); i++) {
            if(mDiaryList.get(i).getIsSleep()) {
                viewHolder.sleepTouch.setBackgroundResource(R.color.item_coffee_background);
            } else {
                viewHolder.sleepTouch.setBackgroundResource(R.color.item_background);
            }
        }
    }

    public void setItemColor(final DiaryViewHolder viewHolder, int selectIndex) {
        if(items[selectIndex].equals("커피 먹은 시간")) {
            viewHolder.sleepTouch.setBackgroundResource(R.color.item_coffee_background);
        } else if(items[selectIndex].equals("술 먹은 시간")) {
            viewHolder.sleepTouch.setBackgroundResource(R.color.item_drinking_background);
        } else if(items[selectIndex].equals("수면약 먹은 시간")) {
            viewHolder.sleepTouch.setBackgroundResource(R.color.item_sleep_drug_background);
        } else if(items[selectIndex].equals("누운 시간")) {
            viewHolder.sleepTouch.setBackgroundResource(R.color.item_sleep_background);
        } else if(items[selectIndex].equals("일어난 시간")) {
            viewHolder.sleepTouch.setBackgroundResource(R.color.item_wake_background);
        } else {
            viewHolder.sleepTouch.setBackgroundResource(R.color.item_background);
        }

        viewHolder.sleepType.setText(items[selectIndex]);
    }
}


