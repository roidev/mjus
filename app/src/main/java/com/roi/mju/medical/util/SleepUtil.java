package com.roi.mju.medical.util;

import android.util.Log;

public class SleepUtil {
    /**
     * 2019616 -> 20190616 로 바꿔 준다
     * @param year
     * @param month
     * @param date
     * @return
     */
    public static String getReplaceDate(long year, long month, long date) {
        String replaceDate = null;
        StringBuffer sb = new StringBuffer();
        String tmpZero = "0";

        String tmpMonth = "";
        String tmpDate = "";

        if(month < 10) {
            tmpMonth = tmpZero + month;
        } else {
            tmpMonth = month + "";
        }
        if(date < 10) {
            tmpDate = tmpZero + date;
        } else {
            tmpDate = date + "";
        }
        replaceDate =  sb.append(year+"" + "년 ").append(tmpMonth + "월 ").append(tmpDate + "일").toString();
        return replaceDate;
    }

    public static int caculateSleepPercent(int startTime, int endTime, int goTime, int wentTime) {
//        startTime = caculate12Hours(startTime);
//        endTime = caculate12Hours(endTime);
//        goTime = caculate12Hours(goTime);
//        wentTime = caculate12Hours(wentTime);

        if(startTime <= 24 && startTime >= 12) {
            startTime = 24 - startTime;
        }

        if(goTime <= 24 && goTime >= 12) {
            goTime = 24 - goTime;
        }
//        Log.e("jun","startTime = " + startTime);
//        Log.e("jun","endTime = " + endTime);
//        Log.e("jun","goTime = " + goTime);
//        Log.e("jun","wentTime = " + wentTime);


        double all = endTime - startTime;
        double part = wentTime - goTime;
        int percent = (int)(all/part * 100.0);
//        Log.e("jun","caculateSleepPercent = " + percent);
        return percent;
    }

    private static int caculate12Hours(int hour) {
        if(hour >= 12) {
            hour = hour - 12;
        }
        return hour;
    }
}
