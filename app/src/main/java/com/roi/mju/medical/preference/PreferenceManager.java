package com.roi.mju.medical.preference;

import android.content.Context;
import android.content.SharedPreferences;
import com.roi.mju.medical.SleepApplication;
import java.util.HashMap;

public class PreferenceManager {

    // 수면 지침
    private static final String SLEEP_INSTRUCTIONS_PREFERENCE = "sleep_instructions_preference";

    private static final String STROLL_AUTH_PREFERENCE = "stroll_auth_preference";
    private static final String STROLL_ACCOUNT_PREFERENCE = "stroll_account_preference";

    public static final String PREF_KEY_PREFIX = "pref_key_";

    private static PreferenceManager sPreferencesManager;
    private static String sPreferenceName;

    private Context mContext;

    private PreferenceManager() {
        mContext = SleepApplication.Companion.getInstance().getApplicationContext();
    }

    private static PreferenceManager getInstance() {
        if (sPreferencesManager == null) {
            sPreferencesManager = new PreferenceManager();
        }

        return sPreferencesManager;
    }

    public static PreferenceManager getAuth() {
        sPreferenceName = STROLL_AUTH_PREFERENCE;
        return getInstance();
    }

    public static PreferenceManager getAccount() {
        sPreferenceName = STROLL_ACCOUNT_PREFERENCE;
        return getInstance();
    }

    public static PreferenceManager getInstructions() {
        sPreferenceName = SLEEP_INSTRUCTIONS_PREFERENCE;
        return getInstance();
    }


    public static void clearAll() {
        getAuth().clear();
        getAccount().clear();
        getInstructions().clear();
    }


    //----------------------------------------
    //
    //----------------------------------------

    public static final int DEFAULT_VALUE = 0;

    private HashMap<String, Object> mSharedDatas = new HashMap<>();

    private SharedPreferences getPreferences() {
        return mContext.getSharedPreferences(sPreferenceName, Context.MODE_PRIVATE);
    }

    public String getString(String key, String defValue) {
        Object value = mSharedDatas.get(key);
        if (value == null) {
            String s = getPreferences().getString(key, defValue);
            mSharedDatas.put(key, s);
            return s;
        } else {
            return (String) value;
        }
    }

    public int getInt(String key, int defValue) {
        Object value = mSharedDatas.get(key);
        if (value == null) {
            Integer i = getPreferences().getInt(key, defValue);
            mSharedDatas.put(key, i);
            return i;
        } else {
            return (Integer) value;
        }
    }

    public boolean getBoolean(String key, boolean defValue) {
        Object value = mSharedDatas.get(key);
        if (value == null) {
            Boolean b = getPreferences().getBoolean(key, defValue);
            mSharedDatas.put(key, b);
            return b;
        } else {
            return (Boolean) value;
        }
    }

    public float getFloat(String key, float defValue) {
        Object value = mSharedDatas.get(key);
        if (value == null) {
            Float f = getPreferences().getFloat(key, defValue);
            mSharedDatas.put(key, f);
            return f;
        } else {
            return (Float) value;
        }
    }

    public double getDouble(String key, double defValue) {
        Object value = mSharedDatas.get(key);
        if (value == null) {
            Double f = Double.longBitsToDouble(getPreferences().getLong(key, Double.doubleToLongBits(defValue)));
            mSharedDatas.put(key, f);
            return f;
        } else {
            return (Double) value;
        }
    }

    public long getLong(String key, long defValue) {
        Object value = mSharedDatas.get(key);
        if (value == null) {
            Long l = getPreferences().getLong(key, defValue);
            mSharedDatas.put(key, l);
            return l;
        } else {
            return (Long) value;
        }
    }

    public String getValue(String key, String defValue) {
        return getString(key, defValue);
    }

    public boolean setString(String key, String value) {
        try {
            Object object = mSharedDatas.put(key, value);
            String previous = object == null ? null : (String) object;
            if (previous == null || !previous.equals(value)) {
                editor().putString(key, value).commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean setInt(String key, int value) {
        try {
            Object object = mSharedDatas.put(key, value);
            Integer previous = (object == null || !(object instanceof Integer)) ? null : (Integer) object;
            if (previous == null || previous != value) {
                editor().putInt(key, value).commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean setBoolean(String key, boolean value) {
        try {
            Object object = mSharedDatas.put(key, value);
            Boolean previous = (object == null || !(object instanceof Boolean)) ? null : (Boolean) object;
            if (previous == null || previous != value) {
                editor().putBoolean(key, value).commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean setFloat(String key, float value) {
        try {
            Object object = mSharedDatas.put(key, value);
            Float previous = (object == null || !(object instanceof Float)) ? null : (Float) object;
            if (previous == null || previous != value) {
                editor().putFloat(key, value).commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean setDouble(String key, double value) {
        try {
            Object object = mSharedDatas.put(key, value);
            Double previous = (object == null || !(object instanceof Double)) ? null : (Double) object;
            if (previous == null || previous != value) {
                editor().putLong(key, Double.doubleToRawLongBits(value)).commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean setLong(String key, Long value) {
        try {
            Object object = mSharedDatas.put(key, value);
            Long previous = (object == null || !(object instanceof Long)) ? null : (Long) object;
            if (previous == null || previous != value) {
                editor().putLong(key, value).commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean setValue(String key, String value) {
        return setString(key, value);
    }

    public boolean remove(String key) {
        try {
            editor().remove(key).commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        mSharedDatas.remove(key);

        return true;
    }

    public boolean clear() {
        try {
            editor().clear().commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        mSharedDatas.clear();

        return true;
    }

    private SharedPreferences.Editor editor() {
        return getPreferences().edit();
    }
}
